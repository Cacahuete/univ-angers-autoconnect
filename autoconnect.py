#!/usr/bin/python

import os
import sys
import getopt
import requests
import sqlite3
import shutil
import gnomekeyring


#code retour:
#	1: page inaccessible
#	2: mauvais identifiants de connexion
#	3: mauvais arguments

pathChromium = os.environ['HOME']+'/.config/chromium/Default/Login Data'
pathChrome = os.environ['HOME']+'/.config/chrome/Default/Login Data'

class User:

    def __init__(self, login, password):
        self.login = login
        self.password = password


def readDatabase( path ) :
    if not os.path.isfile(path) :
        error("Le fichier \""+path+"\" n'existe pas, selectionner une autre option.")
    pathTmp = os.environ['HOME']+'/Login Data'
    shutil.copy2(path, pathTmp)
    conn = sqlite3.connect(pathTmp)
    c = conn.cursor()
    c.execute("""SELECT username_value, CAST(password_value as TEXT) FROM logins WHERE origin_url LIKE 'https%univ-angers%'""")
    row = c.fetchone()
    if ( row != None ) :
        user = User(row[0],row[1])
    else :
        user = None
    c.close()
    os.remove(pathTmp)
    return user

def readGnomeKeyring() :
    try :
        for keyring in gnomekeyring.list_keyring_names_sync():
            for id in gnomekeyring.list_item_ids_sync(keyring):
                item = gnomekeyring.item_get_info_sync(keyring, id)
                attr = gnomekeyring.item_get_attributes_sync(keyring, id)
                if attr and attr.has_key('username_value'):
                    if "univ-angers" in item.get_display_name() and "https" in item.get_display_name() :
                         return User(attr['username_value'],item.get_secret())
    except gnomekeyring.IOError :
        error("Le trousseau de cles est verouille")
    return None

def error( message ) :
    print message
    sys.exit(2)


def help() :
    print "autoconnect [OPTION] :\n\
\t-h,--help\n\t\tAffiche l'aide\n\
\t--chrome\n\t\tUtilise la base de donnnee de chrome\n\
\t--chromium\n\t\tUtilise la base de donnnee de chromium\n\
\t--keyring\n\t\tUtilise la base de donnnee interne a Gnome\n\
\t-u,--user=WORD\n\t\tDefinition manuel de l'identifiant de connexion\n\
\t-p,--password=WORD\n\t\tDefinition manuel du mot de passe de connexion"
    sys.exit(3)



try:
    opts, args = getopt.getopt(sys.argv[1:],"hu:p:",["help","chrome","chromium","keyring","user=","password="])
except getopt.GetoptError :
    error("mauvais arguments --help") 
for opt, arg in opts :
    if opt in ("-h","--help") :
        help()
    elif opt in ("--chrome") :
        user = readDatabase( pathChrome )
    elif opt in ("--chromium") :
        user = readDatabase( pathChromium )
    elif opt in("--keyring") :
        user = readGnomeKeyring()
    elif opt in ("-u","--user") :
	login = arg
    elif opt in ("-p","--password") :
        password = arg
    else :
        help()

if 'user' not in locals() :
    if 'login' in locals() and 'password' in locals() :
        user = User(login,password)
    else :
        help()
elif user == None :
    error("Pas d'identifiant trouve dans la base de donnee")
    sys.exit(1)


payload = {'auth_user':user.login,'auth_pass':user.password,'redirurl':'','accept':'validez'}
length = 49+len(user.login)+len(user.password)

headers ={'Host':' cerbere2.info-ua:8001',
'User-Agent':' Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0',
'Accept':' text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
'Accept-Language':' fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3',
'Accept-Encoding':' gzip, deflate',
'Referer':' https://cerbere2.info-ua:8001/',
'Content-Length':' 126',
'Content-Type': 'text/plain; charset=UTF-8',
'Connection': 'keep-alive',
'Pragma': 'no-cache',
'Cache-Control': 'no-cache',
'Content-Type': 'application/x-www-form-urlencoded',
'Content-Length': length,
}

try :
    r = requests.post('https://cerbere2.info-ua:8001/', data=payload,headers=headers,verify=False)
except requests.exceptions.ConnectionError :
    print 'page web inaccessible'
    sys.exit(1)

if (r.status_code!=200) :
    print 'page web inaccessible'
    sys.exit(1)
if 'Votre authentification a &eacute;chou&eacute;' in r.text:
    print 'bad login'
    sys.exit(2)
else:
    print 'enjoy'

sys.exit(0)
